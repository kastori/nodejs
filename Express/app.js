//비동기로 실행
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var router = require('./router/index')
var passport = require('passport')
var localStrategy = require('passport-local').Strategy
var session = require('express-session')
var flash = require('connect-flash') //서버 메세지를 쉽게확인

app.listen(3000, function () {
    console.log("start!! express serer on port 3000")
})

//static경로 등록
app.use(express.static('public'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//view engine ejs사용
app.set('view engine', 'ejs')

//session 사용
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}))
//passport 사용
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

//router 사용
app.use(router)
