## 설치(관리자 권한)
```npm install pm2 -g```

## 시작
```pm2 start main.js```

## 모니터링(강제로 종료해도 재실행됨)
```pm2 monit```

## stop
```pm2 stop main```

## 변경사항 자동 재실행
```pm2 start main.js --watch```

## 문제점 확인
```pm2 log```