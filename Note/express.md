## npm패키지 생성
```npm init```

## express 설치
```npm install express --save```
--save package.json에 express에 의존하고있다고 저장

## 공식주소
https://expressjs.com/

## 노드몬(변경사항이 있을시 자동으로 재실행)
```npm install nodemon -g```

## body-parser 
```post body를 받을수있는```
```npm install body-parser --save```

## ejs
```npm install ejs --save```
## mysql 설치
```npm install mysql --save```

```
DB USER테이블 

-- test.`user` definition

CREATE TABLE `user` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `pw` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

## passport
```npm install passport passport-local express-session connect-flash --save-dev```
