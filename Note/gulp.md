
# Gulp

### 시작하기 전
- gulp를 시작하려면 node.js 가 설치 되어있어야 한다.

```shell
node --version
npm --version
npx --version
```

### gulp CLI 설치
- 노드가 설치되어 있으면 npm과 npx 자동으로 설치된다. 

```shell
컴퓨터 모든 곳에서 사용하는 옵션 -g
npm install -g gulp-cli
```

### 프로젝트 폴더를 만들고 폴더 안으로 들어가기
- [glup 공식 가이드 문서](https://gulpjs.com/docs/en/getting-started/quick-start)를 보면 npx를 사용하여 프로젝트 폴더를 만든다. npx는 한번만 사용할 작업을 할 때 사용한다고 생각하면 좋을 것 같다. 
```shell
npx mkdirp my-project
cd my-project
```

### 프로젝트 폴더안에 package.json 파일 생성
- npm init 명령어를 실행시키면 프로젝트 이름, 버전, 라이센스등 프로젝트 관련 정보를 package.json 파일안에 가입할 수 잇는 창이 나타나고 프로젝트에 대한 정보를 입력하면 된다.

### 개발모드로 gulp설치
- 아래 명령어 대로 설치하면 프로젝트 폴터안에 gulp가 설치되고 package.json 파일에 package가 기록된다.
```shell
npm install --save-dev gulp
```
- package.json 파일 안에 devDependecies에 gulp가 적혀있는걸 볼수있다.

### gulp 버전확인
- 아래 명령어를 쳤을 때 버전이 나오면 gulp.js를 사용할 준비가 된 것이다.
```shell
gulp --version
```
