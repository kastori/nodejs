const fs = require('fs');

/* 
//readFileSync(A B C)
console.log('A');
var result = fs.readFileSync('sample.txt', 'utf-8');
console.log(result)
console.log('C')
*/

//readFile 순차적 실행 X
console.log('A');
fs.readFile('sample.txt', 'utf-8', function (err, result) {
    console.log(result)
});
console.log('C')