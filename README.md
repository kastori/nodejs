## 0.JavaScript의 기본문법 & Node.js의 주요 기능 & 웹앱
[인프런 WEB2 - Node.js](https://www.inflearn.com/course/web2-node-js/dashboard)

## Express 
[인프런 Node.js 웹개발로 알아보는 백엔드 자바스크립트의 이해](https://www.inflearn.com/course/node-js-%EC%9B%B9%EA%B0%9C%EB%B0%9C/dashboard)


## Gulp

[참고사이트](https://www.opentutorials.org/module/4533/27464)

## REST API

- [유튜브 영상](https://www.youtube.com/watch?v=VWEJ-GhjU4U)
- [express-api-starter](https://github.com/w3cj/express-api-starter)