const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');

require('dotenv').config({ path: '../.env' });
require('./auth/passport');

require('./models/User');
const mongoose = require('mongoose');

const middlewares = require('./middlewares');
const api = require('./api');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.use('/api/v1/', api);
mongoose
  .connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    serverSelectionTimeoutMS: 2500
  })
  .then(() => console.log('Successfully connected to mongodb!'))
  .catch((e) => console.error(e));

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
