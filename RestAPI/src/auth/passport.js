const passport = require('passport');
const passportJwt = require('passport-jwt');

const { ExtractJwt } = passportJwt;
const StrategyJwt = passportJwt.Strategy;
const User = require('../models/User');

passport.use(
  new StrategyJwt({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET,
  },
  ((jwtPayload, done) => User.findOne({ username: jwtPayload.username })
    .then((user) => done(null, user))
    .catch((err) => done(err))))

);
