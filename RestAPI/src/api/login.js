const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/User');

const router = express.Router();
router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  const userWithUsername = await User.findOne({ username }).catch((err) => {
    console.log('Error: ', err);
  });

  if (!userWithUsername) {
    return res.json({
      message: 'fail',
      err: 'username or passowrd does not match!'
    });
  }
  bcrypt.compare(password, userWithUsername.password, (err, isMatch) => {
    if (err) return res.json({ err });

    if (isMatch) {
      const jwtToken = jwt.sign({ id: userWithUsername.id, username: userWithUsername.username }, process.env.JWT_SECRET);
      return res.json({ massage: 'success', token: jwtToken });
    }
    return res.json({ message: 'fail', err: 'passowrd does not match!' });
  });
});
module.exports = router;
