const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const User = require('../models/User.js');

const router = express.Router();

router.post('/register', async (req, res) => {
  const {
    name, username, password, password2
  } = req.body;
  const errors = [];
  const currentUsers = await User.find({});

  // Check required fields
  if (!name || !username || !password || !password2) {
    errors.push({ msg: 'Please fill in all fields...' });
  }

  // Check passwords match
  if (password !== password2) {
    errors.push({ msg: 'Passwords do not match...' });
  }

  // Password at least 6 characters
  if (password.length < 6) {
    errors.push({ msg: 'Password should be at least 6 characters...' });
  }

  if (errors.length > 0) {
    res.json({
      message: 'fail',
      errors
    });
  } else {
    // Validation Passed
    User.findOne({ username }).then((user) => {
      if (user) {
        // User exists
        errors.push({ msg: 'Username is already registered' });
        res.send({
          message: 'fail',
          errors
        });
      } else {
        // Check if first user that's created.
        User.find({}).then((user) => {
          let userGroup = '';
          if (user.length < 1) {
            userGroup = 'Administrator';
          } else {
            userGroup = 'User';
          }
          const newUser = new User({
            name,
            username,
            password,
            group: userGroup,
          });
            // Hash Password
          bcrypt.genSalt(10, (error, salt) => bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            // Set password to hashed
            newUser.password = hash;
            // Save new User
            newUser
              .save()
              .then((user) => {
                console.log(`register: ${user}`);
                res.json({
                  message: 'success'
                });
              })
              .catch((e) => {
                res.json({
                  message: 'fail',
                  err: e
                });
              });
          }));
        });
      }
    });
  }
});

module.exports = router;
